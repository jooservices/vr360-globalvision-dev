
![Globalvision](http://vr360.globalvision.ch/assets/images/gv_logo.png)
----------
VR360
===================

Changelogs
**Version 2.0.0**
 - **Refactored whole system** with MVC supported
 - Upgraded to krpano version 1.19
 - Use Javascript object
 - Implemented SEO for tour view
 - Move data.json into database
 - Database optimized
 - UI improved
 - New session system
 - New configuration system
 - Email sending
 - Searching
